package assignment;

class Exam {

	static int mat = MyUserInput.readNumber("Enter your Maths marks: \n");
	static int sc = MyUserInput.readNumber("Enter your Science marks: \n");
	static int eng = MyUserInput.readNumber("Enter your English marks: \n");
	static int avg;
	static String grade;

	public static void main(String[] args) {

		avg = (mat + sc + eng) / 3;

		if (avg >= 50) {
			grade = "You have passed the examination.";

		} else {
			grade = "You have failed the examination.";

		}

		System.out.println(grade);

	}

}

