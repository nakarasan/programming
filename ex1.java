package assignment;

import java.util.Scanner;

public class ex1 {
	public static void main(String[] args) {

	    Scanner input = new Scanner(System.in);
	    System.out.println("Enter First Name: ");
	    String firstName = input.nextLine();
	    System.out.println("Enter Last Name: ");
	    String lastName = input.nextLine();
	    System.out.println("Enter Home Address: ");
	    String homeAddress = input.nextLine();

	    Customer cus = new Customer;

	    System.out.println("\nWelcome: ");
	    System.out.print(cus.getFirstName() + cus.getLastName());
	    System.out.println("\n Your Shipping Address: ");
	    System.out.print(cus.getHomeShippingAddress());

	    List<Customer> customer = new ArrayList<Customer>();

	    customer.add(cus);

	    // Output the list contents
	    printList(customer);

	}

	public static void printList(List<Customer> list) {

	    System.out.println("Customers: ");

	    for (Customer customer : list) {
	        System.out.printf("%s", customer);
	    }
	    System.out.println();
	}
	
}
